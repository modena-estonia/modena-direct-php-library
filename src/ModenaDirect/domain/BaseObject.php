<?php

namespace Modena\Payment\src\ModenaDirect\domain;

class BaseObject
{
    /**
     * Convert Object to Array
     * json_decode because (array) changes key-s
     * @return array
     */
    public function toArray()
    {
        return json_decode(json_encode($this), true);
    }
}
