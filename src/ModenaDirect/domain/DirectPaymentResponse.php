<?php

namespace Modena\Payment\src\ModenaDirect\domain;

class DirectPaymentResponse
{
    /**
     * Response signature verified & transaction was successful
     */
    const STATUS_SUCCESS = 1;
    /**
     * Response signature verified, but transaction was canceled
     */
    const STATUS_CANCEL = 0;
    /**
     * Response signature could not be verified
     */
    const STATUS_ERROR = -1;

    protected $status;
    protected $orderId;
    protected $transactionId;
    protected $total;

    public function __construct($status = null, $stamp = null, $transactionId = null, $total = null)
    {
        $this->status = $status;
        $this->orderId = $stamp;
        $this->transactionId = $transactionId;
        $this->total = $total;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setOrderId($id)
    {
        $this->orderId = $id;
    }

    public function getOrderId()
    {
        return $this->orderId;
    }

    public function setTransactionId($id)
    {
        $this->transactionId = $id;
    }

    public function getTransactionId()
    {
        return $this->transactionId;
    }

    public function setTotal($total)
    {
        $this->total = $total;
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function isSuccessful()
    {
        return $this->status === self::STATUS_SUCCESS;
    }
}