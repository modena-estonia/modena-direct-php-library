<?php

namespace Modena\Payment\src\ModenaDirect\domain;

use Modena\Payment\src\ModenaDirect\util\Util;
use Modena\Payment\src\ModenaDirect\config\Config;
use Modena\Payment\src\ModenaDirect\config\Fields;
use Modena\Payment\src\ModenaDirect\config\Services;

class DirectPaymentRequest
{
    protected $service;
    protected $senderId;
    protected $stamp;
    protected $amount;
    protected $return;
    protected $dateTime;
    protected $order;
    protected $version;
    protected $option;
    protected $curr;
    protected $ref;
    protected $msg;
    protected $cancel;
    protected $callback;
    protected $lang;
    protected $mac;

    public function __construct(
        $senderId,
        $stamp,
        $amount,
        $return,
        $order,
        $option,
        $optionalParams
    ) {
        $this->senderId = $senderId;
        $this->stamp = $stamp;
        $this->amount = $amount;
        $this->return = $return;
        $this->dateTime = new \DateTime('now', new \DateTimeZone('UTC'));
        $this->order = $order;
        $this->option = $option;
        if (isset($optionalParams['MDN_SERVICE'])) $this->service = $optionalParams['MDN_SERVICE'];
        else $this->service = Services::DIRECT_PAYMENT_REQUEST;
        if (isset($optionalParams['MDN_VERSION'])) $this->version = $optionalParams['MDN_VERSION'];
        else $this->version = Config::VERSION;
        if (isset($optionalParams['MDN_CURR'])) $this->curr = $optionalParams['MDN_CURR'];
        else $this->curr = Config::DEFAULT_CURRENCY;
        if (isset($optionalParams['MDN_REF'])) $this->ref = $optionalParams['MDN_REF'];
        else $this->ref = '';
        if (isset($optionalParams['MDN_MSG'])) $this->msg = $optionalParams['MDN_MSG'];
        else $this->msg = '';
        if (isset($optionalParams['MDN_CANCEL'])) $this->cancel = $optionalParams['MDN_CANCEL'];
        else $this->cancel = '';
        if (isset($optionalParams['MDN_CALLBACK']) && Util::isValidURL($this->callback)) $this->callback = $optionalParams['MDN_CALLBACK'];
        else $this->callback = '';
        if (isset($optionalParams['MDN_LANG'])) $this->lang = $optionalParams['MDN_LANG'];
        else $this->lang = Config::DEFAULT_LANG;
    }

    public function setMac($mac)
    {
        $this->mac = $mac;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $parameters[Fields::MDN_SERVICE] = $this->service;
        $parameters[Fields::MDN_SND_ID] = $this->senderId;
        $parameters[Fields::MDN_STAMP] = $this->stamp;
        $parameters[Fields::MDN_AMOUNT] = $this->amount;
        $parameters[Fields::MDN_RETURN] = $this->return;
        $parameters[Fields::MDN_DATETIME] = $this->dateTime->format("Y-m-d\TH:i:sO");
        $parameters[Fields::MDN_ORDER] = json_encode($this->order->toArray());
        $parameters[Fields::MDN_OPTION] = $this->option;
        $parameters[Fields::MDN_VERSION] = $this->version;
        $parameters[Fields::MDN_CURR] = $this->curr;
        $parameters[Fields::MDN_REF] = $this->ref;
        $parameters[Fields::MDN_MSG] = $this->msg;
        $parameters[Fields::MDN_CANCEL] = $this->cancel;
        $parameters[Fields::MDN_CALLBACK] = $this->callback;
        $parameters[Fields::MDN_LANG] = $this->lang;
        $parameters[Fields::MDN_MAC] = $this->mac;
        return $parameters;
    }
}