<?php

namespace Modena\Payment\src\ModenaDirect\domain;

class ClientInfo extends BaseObject
{
    /**
     * @var string
     */
    public $firstName;

    /**
     * @var string
     */
    public $lastName;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $phone;

    /**
     * @var string
     */
    public $country;

    /**
     * @var string
     */
    public $fullAddress;

    /**
     * @var int
     */
    public $postalCode;

    /**
     * @param string $firstName
     * @param string $lastName
     * @param string $email
     * @param string $phone
     * @param string $country
     * @param string $fullAddress
     * @param int $postalCode
     */
    public function __construct($firstName, $lastName, $email, $phone, $country, $fullAddress, $postalCode)
    {
        $this->phone = $phone;
        $this->email = $email;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->country = $country;
        $this->fullAddress = $fullAddress;
        $this->postalCode = $postalCode;

        $this->validate();
    }

    /**
     * @throws \InvalidArgumentException
     */
    private function validate()
    {
        if (!$this->email) {
            throw new \InvalidArgumentException("Required field cannot be empty");
        }

        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            throw new \InvalidArgumentException("Invalid email");
        }
    }
}
