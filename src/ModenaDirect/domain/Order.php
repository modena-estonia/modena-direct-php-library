<?php

namespace Modena\Payment\src\ModenaDirect\domain;

class Order extends BaseObject
{
    /**
     * @var ClientInfo
     */
    public $client;

    /**
     * @var OrderRow[]
     */
    public $orderRows;

    /**
     * @param ClientInfo $client
     * @param OrderRow[] $orderRows
     */
    public function __construct($client, $orderRows)
    {
        $this->client = $client;
        $this->orderRows = $orderRows;

        $this->validate();
    }

    /**
     * @throws \InvalidArgumentException
     */
    private function validate()
    {
        if (!$this->client instanceof ClientInfo) {
            throw new \InvalidArgumentException("Invalid client object");
        }

        foreach ($this->orderRows as $row) {
            if (!$row instanceof OrderRow) {
                throw new \InvalidArgumentException("Invalid orderRows");
            }
        }
    }
}
