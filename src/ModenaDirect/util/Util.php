<?php

namespace Modena\Payment\src\ModenaDirect\util;

use Modena\Payment\src\ModenaDirect\config\Fields;
use Modena\Payment\src\ModenaDirect\config\Services;

class Util
{
    public static function generateModenaDirectMAC($data)
    {
        $mac = '';

        foreach (Services::getFieldsForService($data[Fields::MDN_SERVICE]) as $fieldName)
        {
            // Count the number of bytes in the UTF-8 encoded string
            $mac .= str_pad(strval(strlen($data[$fieldName])), 3, '0', 0) . $data[$fieldName];
        }

        return $mac;
    }

    public static function startsWith($mainString, $prefix) {
        $length = strlen($prefix);
        return substr($mainString, 0, $length) === $prefix;
    }

    public static function endsWith($mainString, $suffix) {
        $length = strlen($suffix);
        if (!$length) return true;
        return substr($mainString, -$length) === $suffix;
    }

    public static function isModenaImageURL($inputURL)
    {
        if (!self::isValidURL($inputURL)) return false;
        $imageFileExtensions = [".png", ".PNG", ".svg", ".SVG", ".jpg", ".JPG", ".jpeg", ".JPEG", ".gif", ".GIF"];
        for ($i = 0; $i < count($imageFileExtensions); $i++) {
            if (self::startsWith($inputURL,'https://docs.buyplan.ee') && self::endsWith($inputURL, $imageFileExtensions[$i])) return true;
            if (self::startsWith($inputURL,'https://cms.modena.ee') && self::endsWith($inputURL, $imageFileExtensions[$i])) return true;
        }
        return false;
    }

    public static function isValidURL($inputURL) {
        if ($inputURL == null) return false;
        if (strlen($inputURL) == 0) return false;
        if (!filter_var($inputURL, FILTER_VALIDATE_URL)) return false;
        return true;
    }
}