<?php

namespace Modena\Payment\src\ModenaDirect;

use Modena\Payment\src\ModenaDirect\config\Config;
use Modena\Payment\src\ModenaDirect\service\DirectPaymentRequestService;
use Modena\Payment\src\ModenaDirect\service\DirectPaymentResponseService;

class ModenaDirect {
    protected $merchantPrivateKey;
    protected $ModenaDirectPublicKey;
    protected $ModenaDirectAPIURL;
    protected $merchantKeySecret;
    protected $merchantRegistryCode;
    protected $version;
    protected $directPaymentRequestService;
    protected $directPaymentResponseService;

    public function __construct(
        $merchantPrivateKey,
        $ModenaDirectPublicKey,
        $ModenaDirectAPIURL,
        $merchantRegistryCode,
        $merchantKeySecret = '',
        $version = Config::VERSION
    ) {
        $this->merchantPrivateKey = $merchantPrivateKey;
        $this->ModenaDirectPublicKey = $ModenaDirectPublicKey;
        $this->ModenaDirectAPIURL = $ModenaDirectAPIURL;
        $this->merchantKeySecret = $merchantKeySecret;
        $this->merchantRegistryCode = $merchantRegistryCode;
        $this->version = $version;

        $this->directPaymentRequestService = new DirectPaymentRequestService($this->ModenaDirectAPIURL, $this->merchantPrivateKey, $this->merchantKeySecret);
        $this->directPaymentResponseService = new DirectPaymentResponseService($this->ModenaDirectPublicKey, $this->merchantRegistryCode);
    }

    public function getResponse($postResponse)
    {
        return $this->directPaymentResponseService->processResponse($postResponse);
    }

    public function getRequestForm($request, $submit = true)
    {
        return $this->directPaymentRequestService->generateRequestForm($request, $submit);
    }
}
