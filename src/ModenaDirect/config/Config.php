<?php

namespace Modena\Payment\src\ModenaDirect\config;

class Config
{
    const VERSION = '008';

    const DEFAULT_CURRENCY = 'EUR';

    const DEFAULT_LANG = 'EST';

    const DEFAULT_ENCODING = 'UTF-8';
}
