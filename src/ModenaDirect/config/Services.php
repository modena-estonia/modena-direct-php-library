<?php

namespace Modena\Payment\src\ModenaDirect\config;

final class Services
{
    const DIRECT_PAYMENT_REQUEST = '2222';
    const DIRECT_PAYMENT_SUCCESS = '2111';
    const DIRECT_PAYMENT_CANCEL = '2911';

    /**
     * @param string $serviceId
     * @return string[]
     */
    public static function getFieldsForService($serviceId)
    {
        switch ($serviceId) {
            case Services::DIRECT_PAYMENT_REQUEST:
                return [
                    Fields::MDN_SERVICE,
                    Fields::MDN_SND_ID,
                    Fields::MDN_STAMP,
                    Fields::MDN_AMOUNT,
                    Fields::MDN_RETURN,
                    Fields::MDN_DATETIME,
                    Fields::MDN_ORDER,
                    Fields::MDN_OPTION,
                    Fields::MDN_VERSION,
                    Fields::MDN_CURR,
                    Fields::MDN_REF,
                    Fields::MDN_MSG,
                    Fields::MDN_CANCEL,
                    Fields::MDN_CALLBACK
                ];
            case Services::DIRECT_PAYMENT_SUCCESS:
                return [
                    Fields::MDN_SERVICE,
                    Fields::MDN_VERSION,
                    Fields::MDN_SND_ID,
                    Fields::MDN_REC_ID,
                    Fields::MDN_STAMP,
                    Fields::MDN_T_NO,
                    Fields::MDN_AMOUNT,
                    Fields::MDN_CURR,
                    Fields::MDN_REC_ACC,
                    Fields::MDN_REC_NAME,
                    Fields::MDN_SND_ACC,
                    Fields::MDN_SND_NAME,
                    Fields::MDN_REF,
                    Fields::MDN_MSG,
                    Fields::MDN_T_DATETIME
                ];
            case Services::DIRECT_PAYMENT_CANCEL:
                return [
                    Fields::MDN_SERVICE,
                    Fields::MDN_VERSION,
                    Fields::MDN_SND_ID,
                    Fields::MDN_REC_ID,
                    Fields::MDN_STAMP,
                    Fields::MDN_REF,
                    Fields::MDN_MSG
                ];
            default:
                throw new \InvalidArgumentException("Unsupported service id: {$serviceId}");
        }
    }

    /**
     * @return string[]
     */
    public static function getAcceptedResponses()
    {
        return [
            self::DIRECT_PAYMENT_SUCCESS,
            self::DIRECT_PAYMENT_CANCEL
        ];
    }
}
