<?php

namespace Modena\Payment\src\ModenaDirect\config;

final class Fields
{
    const MDN_SERVICE = 'MDN_SERVICE';
    const MDN_VERSION = 'MDN_VERSION';
    const MDN_SND_ID = 'MDN_SND_ID';
    const MDN_STAMP = 'MDN_STAMP';
    const MDN_AMOUNT = 'MDN_AMOUNT';
    const MDN_OPTION = 'MDN_OPTION';
    const MDN_CURR = 'MDN_CURR';
    const MDN_REF = 'MDN_REF';
    const MDN_MSG = 'MDN_MSG';
    const MDN_RETURN = 'MDN_RETURN';
    const MDN_CANCEL = 'MDN_CANCEL';
    const MDN_DATETIME = 'MDN_DATETIME';
    const MDN_ORDER = 'MDN_ORDER';
    const MDN_CALLBACK = 'MDN_CALLBACK';
    const MDN_MAC = 'MDN_MAC';
    const MDN_LANG = 'MDN_LANG';
    const MDN_REC_ID = 'MDN_REC_ID';
    const MDN_T_NO = 'MDN_T_NO';
    const MDN_REC_ACC = 'MDN_REC_ACC';
    const MDN_REC_NAME = 'MDN_REC_NAME';
    const MDN_SND_ACC = 'MDN_SND_ACC';
    const MDN_SND_NAME = 'MDN_SND_NAME';
    const MDN_T_DATETIME = 'MDN_T_DATETIME';
}
