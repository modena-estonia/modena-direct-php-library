<?php

namespace Modena\Payment\src\ModenaDirect\service;

use Modena\Payment\src\ModenaDirect\config\Fields;
use Modena\Payment\src\ModenaDirect\config\Services;
use Modena\Payment\src\ModenaDirect\domain\DirectPaymentResponse;
use Modena\Payment\src\ModenaDirect\domain\Response;
use Modena\Payment\src\ModenaDirect\util\Util;
use DateTime;
use DateTimeZone;
use Exception;

class DirectPaymentResponseService
{
    protected $ModenaDirectPublicKey;
    protected $merchantRegistryCode;

    public function __construct($ModenaDirectPublicKey, $merchantRegistryCode)
    {
        $this->ModenaDirectPublicKey = $ModenaDirectPublicKey;
        $this->merchantRegistryCode = $merchantRegistryCode;
    }

    public function processResponse($responseData)
    {
        $verificationResult = $this->verifyResponseSignature($responseData);
        return $this->getResponse($responseData, $verificationResult);
    }

    private function getResponse($responseData, $verificationResult)
    {
        if ($verificationResult) {
            if ($responseData[Fields::MDN_REC_ID] !== $this->merchantRegistryCode) {
                error_log('Registry code mismatch, response processing failed...');
                return new DirectPaymentResponse(DirectPaymentResponse::STATUS_CANCEL, $responseData[Fields::MDN_STAMP], $responseData[Fields::MDN_REF]);
            }

            if (!in_array($responseData[Fields::MDN_SERVICE], Services::getAcceptedResponses())) {
                error_log('Unsupported service code, response processing failed...');
                return new DirectPaymentResponse(DirectPaymentResponse::STATUS_CANCEL, $responseData[Fields::MDN_STAMP], $responseData[Fields::MDN_REF]);
            }

            if ($responseData[Fields::MDN_SERVICE] === Services::DIRECT_PAYMENT_SUCCESS) {
                $status = DirectPaymentResponse::STATUS_CANCEL;
                try {
                    $signatureDateTime = new DateTime($responseData[Fields::MDN_T_DATETIME]);
                    $signatureDateTime = $signatureDateTime->setTimezone(new DateTimeZone('UTC'));
                    $signatureTimestamp = $signatureDateTime->getTimestamp();

                    $now = new DateTime("now", new DateTimeZone('UTC'));
                    $nowTimestamp = $now->getTimestamp();

                    $timestampDifference = $nowTimestamp - $signatureTimestamp;

                    if ($timestampDifference < -120 || $timestampDifference > 1800) {
                        error_log('Signature Timestamp Mismatch... Difference [' . $timestampDifference . ']');
                    } else {
                        $status = DirectPaymentResponse::STATUS_SUCCESS;
                    }
                } catch (Exception $e) {
                    error_log('An exception occurred during the processing of signature timestamp...');
                }
                return new DirectPaymentResponse($status, $responseData[Fields::MDN_STAMP], $responseData[Fields::MDN_REF]);
            } else {
                return new DirectPaymentResponse(DirectPaymentResponse::STATUS_CANCEL, $responseData[Fields::MDN_STAMP], $responseData[Fields::MDN_REF]);
            }
        } else {
            return new DirectPaymentResponse(DirectPaymentResponse::STATUS_ERROR);
        }
    }

    private function verifyResponseSignature($responseData)
    {
        $mac = Util::generateModenaDirectMAC($responseData);

        $key = openssl_get_publickey($this->ModenaDirectPublicKey);
        $result = openssl_verify($mac, base64_decode($responseData[Fields::MDN_MAC]), $key);
        openssl_free_key($key);

        return $result === 1;
    }
}
