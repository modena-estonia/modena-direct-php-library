<?php

namespace Modena\Payment\src\ModenaDirect\service;

use Modena\Payment\src\ModenaDirect\util\Util;

class DirectPaymentRequestService
{
    protected $ModenaDirectAPIURL;
    protected $merchantPrivateKey;
    protected $merchantKeySecret;

    public function __construct($ModenaDirectAPIURL, $merchantPrivateKey, $merchantKeySecret = '')
    {
        $this->ModenaDirectAPIURL = $ModenaDirectAPIURL;
        $this->merchantPrivateKey = $merchantPrivateKey;
        $this->merchantKeySecret = $merchantKeySecret;
    }

    public function generateRequestForm($request, $submit = true)
    {
        $request->setMac($this->getRequestMac($request->toArray()));

        return $this->generateHtml($request, $submit);
    }

    public function generateHtml($request, $submit = true)
    {
        $html = '<form name="modena_direct" action="' . $this->ModenaDirectAPIURL . '" method="POST">';

        foreach ($request->toArray() as $key => $value) {
            $html .= '<input type="hidden" id="' . strtolower($key) . '" name="' . $key . '" value="' . htmlspecialchars($value) . '"/>';
        }

        $html .= '</form>';

        if ($submit) {
            $html .= '<script>document.modena_direct.submit();</script>';
        }

        return $html;
    }

    private function getRequestMac($data)
    {
        $mac = Util::generateModenaDirectMAC($data);

        if ($this->merchantKeySecret) {
            $key = openssl_get_privatekey($this->merchantPrivateKey, $this->merchantKeySecret);
        } else {
            $key = openssl_get_privatekey($this->merchantPrivateKey);
        }

        openssl_sign($mac, $signature, $key);
        openssl_free_key($key);

        return base64_encode($signature);
    }
}